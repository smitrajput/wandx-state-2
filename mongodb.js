const { MongoClient, ObjectID } = require("mongodb");

const id = new ObjectID();
// console.log(id);

const connectionURL = "mongodb://127.0.0.1:27017";
const databaseName = "wandx-state";

MongoClient.connect(
  connectionURL,
  { useNewUrlParser: true },
  (error, client) => {
    if (error) {
      return console.log("Can't connect to db bcoz of: ", error);
    }

    console.log("Connection Successful!");
    const db = client.db(databaseName);

    db.collection("baskets")
      .find({ tokens: { tokenSymbol: "ZRX" } })
      .then(result => console.log(result))
      .catch(error => console.log(error));
  }
);
