const mongoose = require("mongoose");

const betterSchema = new mongoose.Schema({
  userAddress: String,
  basketCreationHash: String,
  token: String,
  txType: String,
  approval_sign: String,
  transfer_sign: String,
  approval_status: String,
  transfer_status: String,
  approval_hash: String,
  transfer_hash: String
});

const Better = mongoose.model("better", betterSchema);

module.exports = Better;
