const mongoose = require("mongoose");

const tableSchema = new mongoose.Schema({
  userAddress: String,
  basketCreationHash: String,
  token: String,
  txType: String,
  signature: String,
  status: String,
  txHash: String
});

const Table = mongoose.model("table", tableSchema);

module.exports = Table;
