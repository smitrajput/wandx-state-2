const express = require("express");
require("./db/mongoose");
const Basket = require("./models/basket");
const Table = require("./models/tables");
const Better = require("./models/better");
const { MongoClient, ObjectID } = require("mongodb");
const request = require("request");

const connectionURL = "mongodb://127.0.0.1:27017";

var Web3 = require("web3");
var receiptdata = [];
var _web3 = new Web3(
  new Web3.providers.HttpProvider(
    "https://ropsten.infura.io/v3/f133272be23d4b718c9e36b693f6d267"
  )
);

const app = express();
const port = 3000;
app.use(express.json()); //parse incoming requests to json

let rawTxns = [],
  preTxns = [],
  txnHashes = [],
  preHashes = [];

function refreshRawTxns() {
  rawTxns = [];

  Better.find(
    {
      $or: [
        {
          $and: [{ approval_status: "unsent" }, { transfer_status: "unsent" }]
        },
        {
          $and: [
            { approval_status: "confirmed" },
            { transfer_status: "unsent" }
          ]
        }
      ]
    },
    (error, res) => {
      if (error) return console.log(error);
      // console.log(res);
      for (let i = 0; i < res.length; ++i) {
        if (
          res[i].approval_status == "unsent" &&
          res[i].transfer_status == "unsent"
        ) {
          rawTxns.push(res[i].approval_sign);
        } else if (
          res[i].approval_status == "confirmed" &&
          res[i].transfer_status == "unsent"
        ) {
          rawTxns.push(res[i].transfer_sign);
        }
      }
      preTxns = rawTxns;
      console.log("rawTxns: ", rawTxns);
    }
  );
}

function refreshTxnHashes() {
  txnHashes = [];

  Better.find(
    {
      $or: [
        { approval_status: "sent" },
        { approval_status: "pending" },
        { transfer_status: "sent" },
        { transfer_status: "pending" }
      ]
    },
    (error, res) => {
      if (error) return console.log(error);
      for (let i = 0; i < res.length; ++i) {
        if (
          res[i].approval_status == "sent" ||
          res[i].approval_status == "pending"
        ) {
          txnHashes.push(res[i].approval_hash);
        } else if (
          res[i].transfer_status == "sent" ||
          res[i].transfer_status == "pending"
        ) {
          txnHashes.push(res[i].transfer_hash);
        }
      }
      preHashes = txnHashes;
      console.log("txnHashes: ", txnHashes);
    }
  );
  //   }
  // );
}

refreshRawTxns();
refreshTxnHashes();

let updateStatusByHash = async (statusHash, newStatus, sign) => {
  console.log("updating status");
  let doc;
  if (
    newStatus == "sent"
    // (doc.txType == "transfer" && newStatus == "confirmed")
  ) {
    doc = await Better.findOne({
      $or: [
        {
          approval_sign: sign
        },
        {
          transfer_sign: sign
        }
      ]
    });
    if (doc) {
      if (doc.txType == "approve") doc.approval_hash = statusHash;
      else doc.transfer_hash = statusHash;
    } else {
      console.log("Doc with sign not found");
    }
  } else {
    if (sign) {
      doc = await Better.findOne({
        $or: [
          {
            approval_sign: sign
          },
          {
            transfer_sign: sign
          }
        ]
      });
    } else {
      doc = await Better.findOne({
        $or: [
          {
            approval_hash: statusHash
          },
          {
            transfer_hash: statusHash
          }
        ]
      });
    }
  }

  if (doc) {
    if (doc.txType == "approve" && newStatus != "confirmed") {
      if (doc.approval_status == "confirmed" && newStatus == "pending") {
        doc.approval_status = "confirmed";
      } else doc.approval_status = newStatus;
    } else if (doc.txType == "approve" && newStatus == "confirmed") {
      doc.approval_status = newStatus;
      doc.txType = "transfer";
      doc.transfer_status = "unsent";
    } else if (doc.txType == "transfer") {
      if (doc.transfer_status == "confirmed" && newStatus == "pending") {
        doc.transfer_status = "confirmed";
      } else doc.transfer_status = newStatus;
    }
    console.log("doc: ", doc);
    await doc.save({ _id: ObjectID(doc._id) });
  } else {
    console.log("Doc not found");
  }
};

var d = new Date();
var epoch = d.getTime() / 1000;

var secondsSinceLastTimerTrigger = epoch % 30; // 600 seconds (10 minutes)
var secondsUntilNextTimerTrigger = 30 - secondsSinceLastTimerTrigger;

function trigger() {
  setTimeout(function() {
    setInterval(function() {
      sendToBlockchain();
      statusCheckAndUpdate();
    }, 30 * 1000);
  }, secondsUntilNextTimerTrigger * 1000);
}

trigger();

async function statusCheckAndUpdate() {
  refreshTxnHashes();
  // setTimeout(function() {}, 3000);
  let localHashes = preHashes;
  console.log("preHashes: ", preHashes);
  console.log("localHashes: ", localHashes);
  for (let i = 0; i < localHashes.length; ++i) {
    await _web3.eth.getTransaction(localHashes[i], async (err, txn) => {
      if (err) return console.log(err);
      // console.log("blockNum", txn.blockNumber);
      try {
        if (txn.blockNumber) {
          await updateStatusByHash(localHashes[i], "confirmed", "");
        } else {
          await updateStatusByHash(localHashes[i], "sent", "");
        }
      } catch (e) {
        console.log("Error fetching transaction from mainnet: ", e);
      }
    });
  }
}

async function sendToBlockchain() {
  // query to get all pending tx
  let txnHash;
  refreshRawTxns();
  let localTxns = preTxns;
  console.log("preTxns: ", preTxns);
  console.log("localTxns: ", localTxns);

  for (let i = 0; i < localTxns.length; ++i) {
    try {
      await _web3.eth
        .sendSignedTransaction(localTxns[i])
        .once("transactionHash", async function(hash) {
          txnHash = hash;
          console.log("hash", hash);
          console.log("localTxns[" + i + "]", localTxns[i]);
          await updateStatusByHash(txnHash, "sent", localTxns[i]);
          console.log("1");
        })
        .once("receipt", async function(receipt) {
          // console.log("receipt: ", receipt);
          // await updateStatusByHash(txnHash, "pending", localTxns[i]);
          console.log("2");
        })
        .once("confirmation", async function(confirmationNumber, receipt) {
          console.log("No: ", confirmationNumber);
          // console.log("Receipt: ", receipt);
          // let update = await updateStatusByHash(txnHash, "confirmed", localTxns[i]);
          // let yulu;
          // if (update) {
          //   yulu = await updateTxType(txnHash, localTxns[i]);
          // }
          await updateStatusByHash(txnHash, "confirmed", localTxns[i]);
          console.log("3");
          // await updateTxType(txnHash, localTxns[i]);
        })
        .on("error", function(err, receipt) {
          if (err) console.log("error: ", err);
          else console.log("Err receipt", receipt);
        })
        .then(function(receipt) {
          // will be fired once the receipt is mined
          console.log("Receipt mined!");
        });
    } catch (e) {
      console.log("Error sending transaction to blockchain: ", e);
    }
  }
}

app.post("/sendToBC", async (req, res) => {
  console.log("sendToBC", typeof req.body);
  let total = Object.keys(req.body).length;
});

app.post("/addBasket", async (req, res) => {
  const basket = req.body;
  try {
    let serverResponse = [];
    for (let i = 0; i < basket.tokens.length; ++i) {
      let field = {
        userAddress: "",
        basketCreationHash: "",
        token: "",
        txType: "",
        approval_sign: "",
        transfer_sign: "",
        approval_status: "",
        transfer_status: "",
        approval_hash: "",
        transfer_hash: ""
      };

      field.userAddress = basket.userAddress;
      field.basketCreationHash = basket.basketCreationHash;
      field.token = basket.tokens[i].token;
      field.txType = basket.tokens[i].txType;
      field.approval_sign = basket.tokens[i].approval_sign;
      field.transfer_sign = basket.tokens[i].transfer_sign;
      field.approval_status = basket.tokens[i].approval_status;
      field.transfer_status = basket.tokens[i].transfer_status;
      field.approval_hash = basket.tokens[i].approval_hash;
      field.transfer_hash = basket.tokens[i].transfer_hash;

      field = new Better(field);
      await field.save();
      serverResponse.push(field);
    }
    res.status(201).send(serverResponse);
  } catch (e) {
    console.log("error ", e);
    res.status(500).send(e);
  }
});

app.get("/basketByID/:id", async (req, res) => {
  try {
    // console.log(req);
    const basket = await Better.find({
      basketCreationHash: req.params.id
    });
    if (!basket) return res.status(404).send();
    res.status(200).send(basket);
  } catch (e) {
    res.status(500).send(e);
  }
});

app.get("/basketsByUser/:id", async (req, res) => {
  try {
    // console.log(req);
    const baskets = await Better.find({
      userAddress: req.params.id
    });
    if (baskets.length == 0) return res.status(404).send();
    res.status(200).send(baskets);
  } catch (e) {
    res.status(500).send(e);
  }
});

app.listen(port, () => {
  console.log("Server running on port: " + port);
});

// app.get("/tokensByBasketAndStatus/:id", async (req, res) => {
//   try {
//     // console.log(req);
//     let tokenArray = [];
//     const basket = await Table.findOne({
//       basketCreationHash: req.params.id
//     });
//     if (!basket) return res.status(404).send();

//     for (let i = 0; i < basket.tokens.length; ++i) {
//       //   // console.log(basket[0].tokens[token]);
//       let _token = basket.tokens[i];
//       // console.log(req.query.status);
//       if (_token.txType.status.statusName == req.query.status) {
//         tokenArray.push(_token.tokenSymbol);
//       }
//       console.log(tokenArray);
//     }
//     res.send(tokenArray);
//   } catch (e) {
//     res.status(500).send(e);
//   }
// });

// app.post("/updateStatusByHash/:id", async (req, res) => {
//   try {
//     // console.log(req);
//     await updateStatusByHash(req.params.id, req.query.newStatus);
//     // if (!basket) return res.status(404).send();

//     // await basket.save({ _id: basket._id });
//     res.send({ msg: "Status successfully updated" });
//   } catch (e) {
//     res.status(500).send(e);
//   }
// });

// app.post("/updateTxTypeByHash/:id", async (req, res) => {
//   try {
//     // console.log(req);
//     await updateTxType(req.params.id, req.query.newTxType);
//     // if (!basket) return res.status(404).send();

//     // await basket.save({ _id: basket._id });
//     res.send({ msg: "txType successfully updated" });
//   } catch (e) {
//     res.status(500).send(e);
//   }
// });

// updateStatusByHash(
//   "0x9944sd91a1f3acecvdfssac1cn2hjdcdkff4bxasew1c0557ef9c1c5414h23f47",
//   "confirmed"
// );

// let updateTxType = async (statusHash, newTxType) => {
//   const basket = await Basket.findOneAndUpdate(
//     {
//       "tokens.txType.status.statusHash": statusHash
//     },
//     {
//       $set: { "tokens.$[elem].txType.txTypeName": newTxType }
//     },
//     { arrayFilters: [{ "elem.txType.status.statusHash": statusHash }] }
//   );
// };

// console.log(rTxns);
// request.post(
//   `https://api.etherscan.io/api?module=proxy&action=eth_sendRawTransaction&hex=${rtxs.rtx1}&apikey=${API_KEY}`,
//   (err, res, body) => {
//     if (err) return console.log("error", err);
//     console.log("res status code: ", res.statusCode);
//     console.log("body: ", body);
//   }
// );

// if (doc.txType == "approve" && newStatus != "confirmed")
//   doc.approval_status = newStatus;
// else if (doc.txType == "approve" && newStatus == "confirmed") {
//   doc.approval_status = newStatus;
//   doc.txType = "transfer";
// } else doc.transfer_status = newStatus;

// let secondUpdate = async (statusHash, newStatus, sign) => {
//   console.log("updating second status");
//   // let doc;
//   Better.findOne(
//     {
//       $or: [
//         {
//           approval_hash: statusHash
//         },
//         {
//           transfer_hash: statusHash
//         }
//       ]
//     },
//     async function(err, doc) {
//       if (err) return console.log(err);

//       if (doc.txType == "approve" && newStatus != "confirmed")
//         doc.approval_status = newStatus;
//       else if (doc.txType == "approve" && newStatus == "confirmed") {
//         doc.approval_status = newStatus;
//         doc.txType = "transfer";
//       } else doc.transfer_status = newStatus;
//       await doc.save({ _id: ObjectID(doc._id) });
//     }
//   );
// };

// let rtxs = {
//   rtx1:
//     "0xf86921843b9aca0082520894bae664a51bf25898bc587f8a1c650bebc2ef4cf387038d7ea4c68000802ba0360f7f0ccfdb5f2cbce09b64b9d3ab8a1d7eba20fc0fb4b88f0e241d14baecb49f9727c031eb484c5eca3d862ea465c5226d77d8d6a78b00b55256d09fb6ff5a" // "0xf88384307832318b307831444344363530303088307841374438433094bae664a51bf25898bc587f8a1c650bebc2ef4cf38f307833384437454134433638303030801ba03c5c6cd3db888535e9c641df52e92b89a5bd02319383d3db17ec4d46a36af36ca03cb030d515a3caa6ac261cefa758c634bbadfeb3a2662b5e19ceaa774aaafd45"
//   // "0xf86a8086d55698372431831e848094f0109fc8df283027b6285cc889f5aa624eac1f55843b9aca008025a009ebb6ca057a0535d6186462bc0b465b561c94a295bdb0621fc19208ab149a9ca0440ffd775ce91a833ab410777204d5341a6f9fa91216a6f3ee2c051fea6a0428"
// };

// let API_KEY = "51YYUZMZYBWWYWIHFHBI9VDV9U1JHGW3IP";

// _web3.eth
//   .sendTransaction({
//     to: "0xbAe664A51Bf25898bC587F8A1C650bebC2EF4CF3",
//     from: "0x8629Bf4070c28271c4201813ADa7fc3777ff95c1",
//     value: "0x38D7EA4C68000"
//   })
//   .on("transactionHash", function(hash) {
//     txnHash = hash;
//     console.log("hash", hash);
//     // await updateStatusByHash(txnHash, "sent");
//   })
//   .on("receipt", function(receipt) {
//     console.log("receipt: ", receipt);
//     // await updateStatusByHash(txnHash, "pending");
//   })
//   .on("confirmation", function(confirmationNumber, receipt) {
//     console.log("No: ", confirmationNumber);
//     console.log("Receipt: ", receipt);
//     // await updateStatusByHash(txnHash, "confirmed");
//   })
//   .on("error", function(err, receipt) {
//     if (err) console.log("error: ", err);
//     else console.log("Err receipt", receipt);
//   });
// _web3.eth
//   .signTransaction({
//     from: "0x8629Bf4070c28271c4201813ADa7fc3777ff95c1",
//     gasPrice: "20000000000",
//     gas: "21000",
//     to: "0xbAe664A51Bf25898bC587F8A1C650bebC2EF4CF3",
//     value: "1000000000000000000",
//     data: ""
//   })
//   .then(console.log);
// }

// app.post("/sendToBC", async (req, res) => {
//   console.log("sendToBC", typeof req.body);
// let str = JSON.stringify(req.body);
// let total = Object.keys(req.body).length;
// trigger(req.body);
// sendToBlockchain(req.body)
// console.log(req.body.serializedTx);
// JSON.parse(req.body).forEach(element => {
// let txnHash;
// _web3.eth
//   .sendSignedTransaction(req.body.rtx1)
// .on("receipt", receipt => {
//   console.log(receipt, Object.keys(req.body).length);
//   console.log("receiptdata", receiptdata, receiptdata.length);
//   if (receipt["status"]) {
//     receiptdata.push(receipt);
//     //  res.send(receipt.json())
//     //res.json({"data":receipt,"status":true});
//     //res.end();
//   }
//   if (total == receiptdata.length) {
//     console.log("receiptdata", receiptdata);

//     res.json({ data: receiptdata });
//     res.end();
//   }
// })
// .on("transactionHash", async function(hash) {
//   txnHash = hash;
//   console.log("hash", hash);
// await updateStatusByHash(txnHash, "pending");
// })
// .on("receipt", async function(receipt) {
//   console.log("receipt: ", receipt);
// await updateStatusByHash(txnHash, "sent");
// })
// .on("confirmation", async function(confirmationNumber, receipt) {
//   console.log("No: ", confirmationNumber);
//   console.log("Receipt: ", receipt);
// await updateStatusByHash(txnHash, "confirmed");
// await updateTxType(txnHash, "transfer");
// })
// .on("error", function(err, receipt) {
//   if (err) console.log("error: ", err);
//   else console.log("Err receipt", receipt);
// });
// });
// });

// let updateTxType = async (statusHash, sign) => {
//   console.log("updating TxType");
//   let doc;
//   doc = await Better.findOne({
//     $or: [
//       {
//         approval_hash: statusHash
//       },
//       {
//         transfer_hash: statusHash
//       }
//     ]
//   });

//   if (doc.txType == "approve" && doc.approval_status == "confirmed") {
//     doc.txType = "transfer";
//     await doc.save({ _id: ObjectID(doc._id) });
//   }
// };
